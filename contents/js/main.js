var app = angular.module("app", []);

app.controller("Controller", function ($scope, $element) {
  
  $scope.preText = '~ $ ';

  $scope.navIndex = '';

  if (document.URL.indexOf('kindle') !== -1) {
    $scope.navIndex = 'kindle';
  }

  if (document.URL.indexOf('.html') !== -1) {
    var content = document.URL.substring(document.URL.indexOf('/', 9));
    content = content.substring(0, content.length - 5)
    $scope.preText = '~' + content + ' $ ';
  }

  $scope.rollText = function(ev) {
    url = angular.element(ev.srcElement)[0].href.toString();
    var content = url.substring(url.indexOf('/', 9));
    if (content.indexOf('.html') === -1) {
      $scope.termText = 'cd ~' + content;
    } else { $scope.termText = 'cd ~' + content.substring(0, content.length - 5); }
  }
});