---
title: f.lux on Ubuntu 13.10
author: Joe Ipson
date: 2014-03-16
template: article.jade
---

Okay, if I'm being perfectly honest, this post isn't about f.lux at all, since I couldn't get that to run the way I wanted in linux with certain temperatures and dual-monitor setup. Instead I'm going to be talking about [Redshift](http://jonls.dk/redshift/).

---

I found Redshift when I was trying to figure out how to get f.lux working properly on Ubuntu 13.10. I tried custom PPAs and the xflux daemon, but couldn't get everything going the way I wanted. I could get the color temperature I wanted with the daemon, but couldn't with the applet. I couldn't get either to change the temperture on my 2nd monitor. Luckily I found Redshift.

First I installed it with apt-get, but it turns out the version of Redshift in the Ubuntu PPA is only 1.7 (Although accoring to [this](https://launchpad.net/ubuntu/+source/redshift), 1.8 will be added to 14.04). We want to be on 1.8 since it's quite a bit better. To get that setup you'll want to follow these steps:

## *__Install prerequisites__*
```
sudo apt-get install build-essential libxcb1-dev libxcb-randr0-dev libx11-dev autoconf autopoint libtool
```
## *__Download and Compile__*
```
wget https://github.com/jonls/redshift/releases/download/v1.8/redshift-1.8.tar.bz2
tar xfv redshift-1.8.tar.bz2
cd redshift-1.8/
./configure
make
sudo make install
```
## *__Configure__*
You'll now need to create a configuration file.
```
touch ~/.config/redshift.conf
```
This is mine. Don't forget to change your 'lat' and 'lon' settings for your city.
```
; Global settings for redshift
[redshift]
; Set the day and night screen temperatures
temp-day=6500
temp-night=3700

; Enable/Disable a smooth transition between day and night
; 0 will cause a direct change from day to night screen temperature. 
; 1 will gradually increase or decrease the screen temperature
transition=1

; Set the screen brightness. Default is 1.0
;brightness=0.9
; It is also possible to use different settings for day and night since version 1.8.
;brightness-day=0.7
;brightness-night=0.4
; Set the screen gamma (for all colors, or each color channel individually)
;gamma=0.8
;gamma=0.8:0.7:0.8

; Set the location-provider: 'geoclue', 'gnome-clock', 'manual'
; type 'redshift -l list' to see possible values
; The location provider settings are in a different section.
location-provider=manual

; Set the adjustment-method: 'randr', 'vidmode'
; type 'redshift -m list' to see all possible values
; 'randr' is the preferred method, 'vidmode' is an older API
; but works in some cases when 'randr' does not.
; The adjustment method settings are in a different section.
adjustment-method=randr

; Configuration of the location-provider:
; type 'redshift -l PROVIDER:help' to see the settings
; ex: 'redshift -l manual:help'
[manual]
lat=PUT YOURS HERE
lon=PUT YOURS HERE

; Configuration of the adjustment-method
; type 'redshift -m METHOD:help' to see the settings
; ex: 'redshift -m randr:help'
; In this example, vidmode is configured to adjust screen 1. 
; Note that the numbering starts from 0, so this is actually the second screen.
;[vidmode]
;screen=1
```
## *__Run__*
```
redshift-gtk
```
If everything turned out okay, you should be able to see the Redshift icon in your panel. Once you click on it, you should see this:
<img src="/img/redshift-gtk.png" style="width:158px; margin-left:auto; margin-right:auto;">

Make sure 'Autostart' is enabled and it'll run as soon as you login.

Let me know in the comments if you have any questions!