---
title: Virtual Desktop Manager
author: Joe Ipson
date: 2016-03-16
template: article.jade
---

If you used Linux or OSX in the past, you've probably made good use of 'virtual desktops' where you can seperate your work into different spaces. For Windows users, there hasn't been a perfect solution for this, and spoiler alert: there still isn't, but it's getting better.

---

I think I've played with most of the different virtual desktop programs for Windows. The best two I've used have been VirtualWin and Dexpot. Both of these have similar features, and what makes them both great is a feature where you can exclude programs (or an entire screen on Dexpot) to be persistant. This is great for working with multiple screens where I want to have documentation or something visible at all times.

VirtualWin is an older program, but it works great (although I had problems with it on Windows 10 and had to switch to Dexpot). But to get it working properly, I had to setup all kinds of rules and expections, so it was kind of a pain to get running.

Dexpot is easier to get up and running right away, but I would start running into some issues after it would be running for a few days. For example, the start menu will just stop opening at all. Another issue I had is that if I changed the number of monitors I had plugged in, it would just crash.

Finally with Windows 10, Microsoft has implemented their own virtual desktop experience, but there are some things I wanted to change, so I wrote '[Virtual Desktop Manager](https://github.com/m0ngr31/VirtualDesktopManager)'.

### About
This program was made for people who are using Windows 10's built-in Virtual Desktops, but who don't like the default key-binding, don't like how you can't cycle through your desktops (If you are on your last one you don't want to hotkey over to the first one 8 times), and don't like not knowing what desktop # they are on.

### Install
There is no installation. Just download the .zip from the Releases tab on the [Github repo](https://github.com/m0ngr31/VirtualDesktopManager), extract it and then run VirtualDesktopManager.exe.

You can use Task Scheduler to make it launch when you login so you don't have to launch it manually every reboot.

### Usage
You can continue to use the default hotkey to change desktops (Ctrl+Win+Right/Left), but you won't get any of the benefit of the program except knowing which desktop you are on. 

I have added a listener to the hotkey of Ctrl+Alt+Right/Left. With this hotkey, you can cycle through your virtual desktops.


### Limitations
 * Due to not wanting to make lots of tray icons, this program only supports up to 9 virtual desktops (it will crash if you go above that).
 * If you try switch between desktops too quickly, windows on different desktops will try to gain focus (you'll see what I mean when you try it out).
 * It needs more testing to see how well it will handle suspend/hibernation events.
 * You will need to relaunch the program if explorer.exe crashes.
 * Hotkeys are statically coded in, so if you want to configure them, you'll have to modify the source.
 * <s>It doesn't handle it very well when you add or create virtual desktops while it's running. You'll need to relaunch it.</s>

I'm trying to work on these issues, but if you have a solution, just throw in a PR and I'll take a look.