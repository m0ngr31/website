---
title: Standing Desk (update w/ 4K TV)
author: Joe Ipson
date: 2016-02-14
template: article.jade
---

I wanted to do a post where I show off my work setup with my home-made standing desk, and how it has evolved in the last year or so.

---

Previously, I had a normal home office like everyone else. I had what I thought was a pretty sweet setup going with 3 monitors running off of my Surface Pro 2. I hadn't even considered a standing desk because who wants to stand all day?

All that changed when I got tired of having pain in my knee from being bent all day under my desk.

I've had issues in the past with my knees: Osgood–Schlatter in one that was a killer when I was a teenager, and now semi-constant pain and popping in the other as an adult. I had a doctor look at it a few years ago and he felt it for 3 seconds and pretty much told me that there was nothing wrong and I was just making up the pain. Thanks for that $200 bill, Doctor. Pretty frustrating to say the least. For the most part it was bearable. If I moved around enough during the day, it wouldn't keep me up at night, but after a while it seemed to be getting worse after I switched jobs (job where I moved around -> working from home). Sitting in a chair all day was just starting to kill me, so I started doing research.

After looking at standing desks off and on for a few months, I decided that it was worth a shot. At first, I just tried putting some cardboard boxes underneath my keyboard and mouse (real classy, I know), and it worked okay, but then I'd get neck pain from looking down all day since I didn't have a way to raise 3 monitors high enough to get them where they needed to be. It was time to build my own.

I tried finding some good plans online, but I couldn't really find one that had a monitor stand like I needed so I tried to just re-create my sitting desk dimensions into a standing desk. I ended up getting a piece of 2'x4'x0.5" board as the main platform, with a piece of 2'x4'x0.5" board as the monitor stand. Then some 4"x4" and 2"x2" posts to create the stand-offs. I also had to get some brackets to mount it on the wall (I'm too lazy to make legs too).

(After the first coat of stain)
![](http://i.imgur.com/wHgq8lc.jpg)


(Everything setup)
![](http://i.imgur.com/S1cMMf9.jpg)

### Pain Update:
No more knee pain! It's been a long time since I've gone this long without an issue, so I've been extremely happy. There was a solid 2 week break-in period for my back and hips. It's a good idea to have something to sit on when your legs start to get tired, and is tall enough to still be able to work.

### Setup Update:
This setup worked great, but after I upgraded to the Surface Pro 4, I decided that I wanted to try out a 4K TV as my only screen. I ended up pulling the trigger on an Amazon daily deal for a Seiki SE42UM TV. For how cheap it was, it has been great. Before I was able to get the active adapter to handle conversion from mini-DisplayPort 1.2 -> HDMI 2.0, I was stuck with 2160p@30Hz, which was pretty painful. But now that I have the adapter, 2160p@60Hz runs great! I haven't had any issues at all. The only thing I can really complain about is that since this is a cheaper Seiki, I don't have full 4:4:4 chroma, but I don't work with a lot of colors, so it's not the end of the world. 

![](http://i.imgur.com/Cjdsisu.jpg)