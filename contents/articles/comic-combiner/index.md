---
title: ComicCombiner - Join .cbz and .cbr files together
author: Joe Ipson
date: 2014-03-15
template: article.jade
---

I recently ran into a situation where I found it easier to have a set of .cbr/cbz files combined into one file, so I created a simple script in Python to combine them for me.

---

There are a few reasons I decided to make this little script:

* There are some comics that are just too short individually that it makes it frustrating to constantly switch through them after you finish one.
* It's a PITA if you have to add 100 comics you're trying to read into a collection on your Kindle.*

*In my particular use case, I'm reading the comics on my Kindle after they've been converted by the brilliant [Kindle Comic Converter](https://github.com/ciromattia/kcc).

The script itself it pretty simple. It will traverse the directory it's in (non-recursive) and extract all the .cbr/cbz files it sees and extract them to a temp directory. Once it's extracted them all, it will create a new .cbz file that you can then put into KCC if you want to use it for your e-book reader.

The only problem I ran into with this solution is that KCC cannot convert a file that is bigger than 320MB (actually a problem with kindlegen), so I've made the script easy to break up a set of files, so you don't end up with enormous .cbz files if you don't want.

You can get/fork my project on [Bitbucket](https://bitbucket.org/m0ngr31/comic-combiner). Just place the .py file in the directory with your comic files and run from the command-line.

```
joinComics.py combinedComics.cbz
```
And when you want to break it up, you can do something like this:
```
joinComics.py "combinedComics 1-25.cbz" 1 25
joinComics.py "combinedComics 26-50.cbz" 26 50
```
Although if you are going to do that, make sure that the filename you created is going to be sorted at the end alphabetically so that it doesn't include the first file you created when you are running it the second time.

Let me know in the comments if you have any questions!