---
title: Getting TrueCrypt to only sync changes
author: Joe Ipson
date: 2014-03-13
template: article.jade
---

If you are anything like me, you love cloud backup services, but you hate how insecure they are. Whether it's the NSA, disgruntled employees, or even hackers, you like the peace of mind that having your sensitive data in a TrueCrypt container brings. Now making it work where you don't have to upload the entire container everytime you make a change is the trick.

---
But luckily this becomes extremely easy as long as your cloud backup service has something called [Delta differencing (or Delta copying)](https://en.wikipedia.org/wiki/Delta_copying). Services like Copy and Dropbox provide this service, whereas OneDrive and Google Drive don't for some reason. If you are looking for more secure services, Wuala and SpiderOak both provide delta copying in addition to several security features making your data outside of TrueCrypt files much more secure. You just have to decide if you want to pay for those services when you can get 20GB easily on Copy.

To get TrueCrypt working with services that support delta copying, just go into the settings and make sure that 'Preserve modification timestamp of file containers' is unchecked.**

<img src="/img/truecrypt.png" style="margin-left:auto; margin-right:auto; width:654px;">

Disabling this setting will tell TrueCrypt to update the timestamp of whatever was modified last in the container, as soon as it is dismounted. If this setting is left checked, the timestamp will never change from the time it was created and it will prohibit whatever service you're using to see that it has changed to trigger an upload. However, having it checked will allow you plausible deniability if you are in a legal situation, so you'll have to weigh out your options. 

** This will make your container sync on services that don't use delta differencing too, it will just force the entire container to upload, instead of just the changed blocks. If you have a large container, this could end up being an issue for you.