---
title: About Whispersync
author: Joe Ipson
date: 2014-06-08
template: kindle-single.jade
---

Whispersync allows you to sync the progress you've made reading a book you've purchased from Amazon between your Kindle and other devices that have Kindle apps. You'll notice that when you copy your own .mobi ebook files to your Kindle and your phone, they won't sync progress with each other, even if they are the exact same file. There is a very simple solution for this.

<span class="more"></span>

When your Kindle or Kindle app on your phone/tablet are looking to see if a file is going to sync progress or not, it looks for 2 EXTH types in the header of the .mobi file:
-   501 - CDEContentType
-   113 - ASIN

And with those two types, certain data needs to be applied to that. For 501, EBOK needs to be the value. For 113, you need to put in a string value. It doesn't need to be an actual valid ASIN number. It seems to work with whatever you want to put in.

Previously, the easiest way to change your files was with Mobi2Mobi (in the MobiPerl suite). But if you aren't looking to do handle your books one-by-one from the command line, you can use Calibre.

To make this work, you'll want to go into your Calibre settings, Output Options, MOBI Output. Make it look like this and hit Apply:

<img src="/img/mobi-settings.png" style="margin-left:auto; margin-right:auto;">

Now when you export your .epub and .pdf files to .mobi it'll add the right information in so you can enjoy progress syncing across your devices. If you already have .mobi files in your Calibre library, simply run another conversion to .mobi and you'll be set.

I'll be going over the proper way to copy these files to your iPhone/iPad, Kindle, Android device, Windows Phone, and Blackberry 10/Playbook devices with individual articles.