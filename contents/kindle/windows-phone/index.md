---
title: Sideload to Windows Phone
author: Joe Ipson
date: 2014-06-09
template: kindle-single.jade
---

Until now, sideloading Kindle ebooks to your Windows Phone 7/8/8.1 device has been impossible. Now it is easy to not only sideload to the Kindle app, but have Whispersync functionality as well.
<span class="more"></span>

Similar to iOS, Windows Phone uses something called Isolated storage for apps. This prevents you (and other apps) from accessing data inside of the folders these apps are installed in. However unlike iOS, it's been impossible to even browse these apps folders from the phone or on a PC. On Windows Phone, the only way to get access to an apps Isolated storage, you can't install it from the Store. It has to be installed from your computer. Luckily for us, there is a way to do this with the Kindle app.

## Things you'll need:
-   A Windows Phone 8 or 8.1 (I haven't personally tried this with WP7)
-   The Windows Phone 8 SDK
-   Windows Phone Power Tools program
-   Ebook(s) that are Whispersync ready
-   USB cable

### Getting everything ready ###
First off, download and install the Windows Phone SDK. You can get it [here](https://dev.windowsphone.com/en-us/downloadsdk). Make sure you read that page so you get everything you need. Once you get everything installed and updated, follow [this tutorial](http://msdn.microsoft.com/en-us/library/windowsphone/develop/ff769508%28v=vs.105%29.aspx) to unlock your phone so we can sideload the Kindle app on it. You shouldn't need to be a registered developer for this, but you will only be able to do 1 phone without a developer account.

### Sideloading ###
Now that you've got the SDK installed and your phone developer unlocked, you'll need to download a program called [Windows Phone Power Tools](https://wptools.codeplex.com/). Once you've installed that, let's download the Kindle xap file. The biggest sacrifice we'll be making to have sideload functionality is that I couldn't find a newer version of the Kindle app. The newest version I could find is v1.2.0.1 which is the most up-to-date version on Windows Phone 7. When this article was written, the current version on WP8 was 2.0.0.6, so I'm not sure how much functionality we're going to be missing out on. If there is something in the newest version that you need, hopefully someone can provide a newer .xap that we can use. It was hard enough finding the version I did. [Here is where I found it](https://mega.co.nz/#!JxMhEK7R!sUkToNOxp5flnBq_yuVFxecuVzDnay3KQSxu5mbyxdA).

Once you've got that, plug your phone into your PC, and go ahead and fire up WPTools.

<img src="/img/wptools1.png" style="margin-left:auto; margin-right:auto;">

Make sure that "Device" is selected. I didn't have to use the "LAUNCH ELEVATED" option, but YMMV.

Next, Click on Install | Update and browse for the Kindle xap you downloaded. Before you hit 'Install', make sure the screen on your phone is on and not at the lock screen.

<img src="/img/wptools2.png" style="margin-left:auto; margin-right:auto;">

Now, if you go to Isolated Storage, you'll see that you can see things! Unfortunately this stuff is pretty worthless to us for now.

<img src="/img/wptools3.png" style="margin-left:auto; margin-right:auto;">

On your phone, launch the Kindle app and log in. Find a free book in the store, or something from your Archived Items and download it. Once it's downloaded, kill the app and go back to WPTools, right-click on the Kindle app, and hit refresh.

<img src="/img/wptools4.png" style="margin-left:auto; margin-right:auto;">

Now we have the right folder that we can throw our .mobi files into. If your .mobi files are [Whispersync ready](/kindle/about-whispersync/), you can use the 'Put File' button to start putting your e-books into the 'kindle' folder. You don't need to worry about covers or anything.

If you are lucky, you should be able to start seeing things like this when you are using different devices:
<img src="/img/wp-kindle.png" style="width:395px; margin-left:auto; margin-right:auto;">

Let me know if the comments if you have any questions about the process.